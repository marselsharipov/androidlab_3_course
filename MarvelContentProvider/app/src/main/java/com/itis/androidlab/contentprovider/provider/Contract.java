package com.itis.androidlab.contentprovider.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public final class Contract {


    public static final String CONTENT_TYPE_APP_BASE = "marvel.";

    public static final String CONTENT_TYPE_BASE =
            "vnd.android.cursor.dir/vnd." + CONTENT_TYPE_APP_BASE;

    public static final String CONTENT_ITEM_TYPE_BASE =
            "vnd.android.cursor.item/vnd." + CONTENT_TYPE_APP_BASE;

    public interface ComicsColumns {
        String TITLE = "title";
        String DESCRIPTION = "description";
        String FORMAT = "format";
        String ISSUENUMBER = "issuenumber";
        String PAGECOUNT = "pagecount";
    }


    public interface ErrorColumns {
        String ERROR_CODE = "err_code";
        String ERROR_MESSAGE = "err_message";
        String ERROR_PROTOCOL = "err_protocol";
    }




    public static final String CONTENT_AUTHORITY = "ru.itis.androidlab.contentprovider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    private static final String PATH_MARVEL = "marvel";
    private static final String PATH_ERROR = "error";

    public static String makeContentType(String id) {
        if (id != null) {
            return CONTENT_TYPE_BASE + id;
        } else {
            return null;
        }
    }

    public static String makeContentItemType(String id) {
        if (id != null) {
            return CONTENT_ITEM_TYPE_BASE + id;
        } else {
            return null;
        }
    }

    public static class Comics implements ComicsColumns, BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MARVEL).build();

        public static final String CONTENT_TYPE_ID = "marvel";

        /**
         * Build a {@link Uri} that references a given tag.
         */
        public static Uri buildTagUri(String tagId) {
            return CONTENT_URI.buildUpon().appendPath(tagId).build();
        }
    }

    public static class Error implements ErrorColumns, BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ERROR).build();

        public static final String CONTENT_TYPE_ID = "error";

        /**
         * Build a {@link Uri} that references a given tag.
         */
        public static Uri buildTagUri(String tagId) {
            return CONTENT_URI.buildUpon().appendPath(tagId).build();
        }
    }
}
