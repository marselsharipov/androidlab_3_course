package com.itis.androidlab.contentprovider.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Helper for managing {@link SQLiteDatabase} that stores data for
 * {@link Provider}.
 */
public class MyDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "marvel.db";
    private static final int DATABASE_VERSION = 3;

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    interface Tables {
        String MARVEL = "comics";
        String ERROR = "error";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.MARVEL + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Contract.Comics.TITLE + " TEXT NOT NULL,"
                + Contract.Comics.DESCRIPTION + " TEXT NOT NULL,"
                + Contract.Comics.FORMAT + " TEXT NOT NULL,"
                + Contract.Comics.PAGECOUNT + " INTEGER NOT NULL,"
                + Contract.Comics.ISSUENUMBER + " REAL NOT NULL)");

        db.execSQL("CREATE TABLE " + Tables.ERROR + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Contract.Error.ERROR_CODE + " REAL NOT NULL,"
                + Contract.Error.ERROR_MESSAGE + " TEXT NOT NULL,"
                + Contract.Error.ERROR_PROTOCOL + " TEXT NOT NULL)");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop tables that have been deprecated.
        db.execSQL("DROP TABLE IF EXISTS " + Tables.MARVEL);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.ERROR);
        onCreate(db);
    }
}