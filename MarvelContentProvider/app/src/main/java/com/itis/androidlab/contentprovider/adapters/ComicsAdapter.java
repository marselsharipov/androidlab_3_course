package com.itis.androidlab.contentprovider.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itis.androidlab.contentprovider.R;
import com.itis.androidlab.contentprovider.models.ListOfComics;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ComicsAdapter extends RecyclerView.Adapter<ComicsAdapter.ViewHolder> {

    private List<ListOfComics> mListOfComics = new ArrayList<>();

    @Override
    public ComicsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_tag, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ComicsAdapter.ViewHolder holder, int position) {
        ListOfComics listOfComics = mListOfComics.get(position);
        holder.mOneElement.setText(String.format(holder.mStringFormatAllTime, "Title: ", listOfComics.getTitle()));
        holder.mTwoElement.setText(String.format(holder.mStringFormatAllTime, "Description: ", listOfComics.getDescription()));
        holder.mThreeElement.setText(String.format(holder.mStringFormatAllTime, "Format release: ", listOfComics.getFormat()));
        holder.mFiveElement.setText(String.format(holder.mStringFormatAllTime, "Issue number in the series: ", listOfComics.getIssueNumber()));
        holder.mFourElement.setText(String.format(holder.mStringFormatAllTime, "Number of pages: ", getPageCount(listOfComics)));
    }


    private String getPageCount(ListOfComics listOfComics) {
        if ((listOfComics.getPageCount()) != 0) {
            return String.valueOf(listOfComics.getPageCount());
        } else return "unknown";
    }

    @Override
    public int getItemCount() {
        return mListOfComics.size();
    }

    public void setmTemp(List<ListOfComics> mTemp) {
        this.mListOfComics = mTemp;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindString(R.string.string_format_all_time) String mStringFormatAllTime;
        @BindView(R.id.one_element) TextView mOneElement;
        @BindView(R.id.five_element) TextView mFiveElement;
        @BindView(R.id.two_element) TextView mTwoElement;
        @BindView(R.id.three_element) TextView mThreeElement;
        @BindView(R.id.four_element) TextView mFourElement;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
