package com.itis.androidlab.contentprovider.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FullWeatherInfo {

    public FullWeatherInfo() {

    }

    @JsonProperty("list")
    private List<ListOfWeather> listofWeather;

    public List<ListOfWeather> getListofWeather() {
        return listofWeather;
    }

    public void setListofWeather(List<ListOfWeather> listofWeather) {
        this.listofWeather = listofWeather;
    }
}

