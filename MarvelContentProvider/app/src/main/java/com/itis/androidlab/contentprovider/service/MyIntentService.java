package com.itis.androidlab.contentprovider.service;

import android.app.IntentService;
import android.content.Intent;

import com.itis.androidlab.contentprovider.models.FullComicsInfo;
import com.itis.androidlab.contentprovider.network.SessionRestManager;
import com.itis.androidlab.contentprovider.provider.Contract;
import com.itis.androidlab.contentprovider.utils.ContentValuesHelper;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;


public class MyIntentService extends IntentService {


    public MyIntentService() {
        super("IntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Call<FullComicsInfo> call = SessionRestManager.getInstance().getRest().getTemperatureByCity();
        try {
            Response<FullComicsInfo> response = call.execute();
            FullComicsInfo fullComicsInfo = response.body();

            getContentResolver().delete(Contract.Comics.CONTENT_URI, null, null);
            getContentResolver().bulkInsert(Contract.Comics.CONTENT_URI, ContentValuesHelper.mapTagToContentValues(fullComicsInfo));

            getContentResolver().insert(Contract.Error.CONTENT_URI, ContentValuesHelper.mapTagToContentValues(response.raw()));


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}