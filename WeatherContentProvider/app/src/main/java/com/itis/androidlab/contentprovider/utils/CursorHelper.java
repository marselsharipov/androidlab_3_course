package com.itis.androidlab.contentprovider.utils;

import android.database.Cursor;

import com.itis.androidlab.contentprovider.models.Temp;
import com.itis.androidlab.contentprovider.provider.WeatherContract;
import com.itis.androidlab.contentprovider.provider.WeatherContract.Weather;

import java.util.ArrayList;
import java.util.List;

public final class CursorHelper {

    private CursorHelper() {
    }

    public static List<Temp> parseTags(Cursor cursor) {
        List<Temp> parsed = new ArrayList<>();

        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            do {
                Temp temp = parseWeather(cursor);
                parsed.add(temp);
            } while (!cursor.isClosed() && cursor.moveToNext());
        }

        return parsed;
    }

    private static Temp parseWeather(Cursor cursor) {
        Temp temp = new Temp();

        temp.setTempEve(cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherContract.Weather.TEMP_EVE)));
        temp.setTempMax(cursor.getDouble(cursor.getColumnIndexOrThrow(Weather.TEMP_MAX)));
        temp.setTempMin(cursor.getDouble(cursor.getColumnIndexOrThrow(Weather.TEMP_MIN)));
        temp.setTempNight(cursor.getDouble(cursor.getColumnIndexOrThrow(Weather.TEMP_NIGHT)));
        return temp;
    }
}