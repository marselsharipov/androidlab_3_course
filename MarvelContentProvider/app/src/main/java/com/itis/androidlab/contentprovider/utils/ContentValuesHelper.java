package com.itis.androidlab.contentprovider.utils;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.itis.androidlab.contentprovider.models.FullComicsInfo;
import com.itis.androidlab.contentprovider.provider.Contract;
import com.itis.androidlab.contentprovider.provider.Contract.Comics;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;


public final class ContentValuesHelper {

    public static ContentValues[] mapTagToContentValues(@NonNull FullComicsInfo fullComicsInfo) {

        ContentValues[] bulkToInsert;
        List<ContentValues> mValueList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ContentValues cvs = new ContentValues();
            cvs.put(Comics.TITLE, fullComicsInfo.getData().getListOfComics().get(i).getTitle());
            cvs.put(Comics.DESCRIPTION, fullComicsInfo.getData().getListOfComics().get(i).getDescription());
            cvs.put(Comics.FORMAT, fullComicsInfo.getData().getListOfComics().get(i).getFormat());
            cvs.put(Comics.PAGECOUNT, fullComicsInfo.getData().getListOfComics().get(i).getPageCount());
            cvs.put(Comics.ISSUENUMBER, fullComicsInfo.getData().getListOfComics().get(i).getIssueNumber());

            mValueList.add(cvs);
        }

        bulkToInsert = new ContentValues[mValueList.size()];
        mValueList.toArray(bulkToInsert);

        return bulkToInsert;
    }


    public static ContentValues mapTagToContentValues(Response raw) {

        ContentValues cv = new ContentValues();

        cv.put(Contract.Error.ERROR_CODE, raw.code());
        cv.put(Contract.Error.ERROR_MESSAGE, raw.message());
        cv.put(Contract.Error.ERROR_PROTOCOL, raw.protocol().toString());

        return cv;
    }
}
