package com.itis.androidlab.contentprovider.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public final class WeatherContract {


    public static final String CONTENT_TYPE_APP_BASE = "weather.";

    public static final String CONTENT_TYPE_BASE =
            "vnd.android.cursor.dir/vnd." + CONTENT_TYPE_APP_BASE;

    public static final String CONTENT_ITEM_TYPE_BASE =
            "vnd.android.cursor.item/vnd." + CONTENT_TYPE_APP_BASE;

    public interface WeatherColumns {
        String TEMP_NIGHT = "temp_night";
        String TEMP_MAX = "temp_max";
        String TEMP_MIN = "temp_min";
        String TEMP_EVE = "temp_eve";
    }

    public interface ErrorColumns {
        String ERROR_CODE = "err_code";
        String ERROR_MESSAGE = "err_message";
        String ERROR_PROTOCOL = "err_protocol";
    }


    public static final String CONTENT_AUTHORITY = "ru.itis.androidlab.contentprovider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    private static final String PATH_WEATHER = "weather";
    private static final String PATH_ERROR = "error";

    public static String makeContentTypeWeather(String id) {
        if (id != null) {
            return CONTENT_TYPE_BASE + id;
        } else {
            return null;
        }
    }

    public static String makeContentItemTypeWeather(String id) {
        if (id != null) {
            return CONTENT_ITEM_TYPE_BASE + id;
        } else {
            return null;
        }
    }

    public static class Weather implements WeatherColumns, BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_WEATHER).build();

        public static final String CONTENT_TYPE_ID = "weather";

        /**
         * Build a {@link Uri} that references a given tag.
         */
        public static Uri buildTagUri(String tagId) {
            return CONTENT_URI.buildUpon().appendPath(tagId).build();
        }
    }

    public static class Error implements ErrorColumns, BaseColumns{
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ERROR).build();

        public static final String CONTENT_TYPE_ID = "error";

        /**
         * Build a {@link Uri} that references a given tag.
         */
        public static Uri buildTagUri(String tagId) {
            return CONTENT_URI.buildUpon().appendPath(tagId).build();
        }
    }
}
