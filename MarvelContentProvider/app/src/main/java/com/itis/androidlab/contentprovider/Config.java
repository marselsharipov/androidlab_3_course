package com.itis.androidlab.contentprovider;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class Config {
    public static final String MARVEL_ENDPOINT_DEBUG = "http://gateway.marvel.com/";
    public static final String MARVEL_ENDPOINT_RELEASE = MARVEL_ENDPOINT_DEBUG;
    public static final String PUBLIC_KEY = "c8c1fe60dac0956e63b4695c29a28081";
    public static final String PRIVATE_KEY = "1ad04fdd5f25244f74ef548d2c7fd848e472a3e7";

    public static final String TS = ts();
    public static final String HASH = hash();


    private static String ts() {
        Date now = new Date();
        return String.valueOf(now.getTime());
    }

    private static String hash() {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String toHash = ts() + PRIVATE_KEY + PUBLIC_KEY;
        assert md != null;
        return new BigInteger(1, md.digest(toHash.getBytes())).toString(16);
    }
}
