package com.itis.androidlab.contentprovider.utils;

import android.database.Cursor;

import com.itis.androidlab.contentprovider.models.ListOfComics;
import com.itis.androidlab.contentprovider.provider.Contract.Comics;

import java.util.ArrayList;
import java.util.List;

public final class CursorHelper {

    private CursorHelper() {
    }

    public static List<ListOfComics> parseTags(Cursor cursor) {
        List<ListOfComics> parsed = new ArrayList<>();

        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            do {
                ListOfComics listOfComics = parseWeather(cursor);
                parsed.add(listOfComics);
            } while (!cursor.isClosed() && cursor.moveToNext());
        }

        return parsed;
    }

    private static ListOfComics parseWeather(Cursor cursor) {
        ListOfComics listOfComics = new ListOfComics();

        listOfComics.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(Comics.DESCRIPTION)));
        listOfComics.setFormat(cursor.getString(cursor.getColumnIndexOrThrow(Comics.FORMAT)));
        listOfComics.setIssueNumber(cursor.getDouble(cursor.getColumnIndexOrThrow(Comics.ISSUENUMBER)));
        listOfComics.setPageCount(cursor.getInt(cursor.getColumnIndexOrThrow(Comics.PAGECOUNT)));
        listOfComics.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(Comics.TITLE)));

        return listOfComics;
    }
}