package com.itis.androidlab.contentprovider.network;


import com.itis.androidlab.contentprovider.models.FullWeatherInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherRest {
    @GET("/data/2.5/forecast/daily")
    Call<FullWeatherInfo> getTemperatureByCity(@Query("q") String cityName, @Query("units") String units, @Query("cnt") int cnt);
}
