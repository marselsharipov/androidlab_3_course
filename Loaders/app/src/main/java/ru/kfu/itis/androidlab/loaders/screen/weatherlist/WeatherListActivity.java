package ru.kfu.itis.androidlab.loaders.screen.weatherlist;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.kfu.itis.androidlab.loaders.R;
import ru.kfu.itis.androidlab.loaders.model.City;
import ru.kfu.itis.androidlab.loaders.screen.general.LoadingDialog;
import ru.kfu.itis.androidlab.loaders.screen.general.LoadingView;
import ru.kfu.itis.androidlab.loaders.screen.general.SimpleDividerItemDecoration;
import ru.kfu.itis.androidlab.loaders.screen.weather.RetrofitWeatherLoaderBerlin;
import ru.kfu.itis.androidlab.loaders.screen.weather.RetrofitWeatherLoaderKazan;
import ru.kfu.itis.androidlab.loaders.screen.weather.RetrofitWeatherLoaderLondon;
import ru.kfu.itis.androidlab.loaders.screen.weather.RetrofitWeatherLoaderMoscow;
import ru.kfu.itis.androidlab.loaders.screen.weather.RetrofitWeatherLoaderWashington;
import ru.kfu.itis.androidlab.loaders.screen.weather.WeatherActivity;


public class WeatherListActivity extends AppCompatActivity implements CitiesAdapter.OnItemClick {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty)
    View mEmptyView;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout mSwipeRefresh;

    private CitiesAdapter mAdapter;

    private LoadingView mLoadingView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_list);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this, false));
        mAdapter = new CitiesAdapter(getInitialCities(), this);
        mRecyclerView.setAdapter(mAdapter);
        mLoadingView = LoadingDialog.view(getSupportFragmentManager());

        loadWeather(false);
        refresh();
        /**
         * TODO : task
         *
         * 1) Load all cities forecast using one or multiple loaders
         * 2) Try to run these requests as most parallel as possible
         * or better do as less requests as possible
         * 3) Show loading indicator during loading process
         * 4) Allow to update forecasts with SwipeRefreshLayout
         * 5) Handle configuration changes
         *
         * Note that for the start point you only have cities names, not ids,
         * so you can't load multiple cities in one request.
         *
         * But you should think how to manage this case. I suggest you to start from reading docs mindfully.
         */
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private void refresh() {
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }

            void refreshItems() {
                loadWeather(true);
                //filling();
                onItemsLoadComplete();
            }

            void onItemsLoadComplete() {
                mSwipeRefresh.setRefreshing(false);
            }
        });
    }


    @Override
    public void onItemClick(@NonNull City city) {
        reloadWeatherCity(city);
    }

    private void reloadWeatherCity(City city) {
        mLoadingView.showLoadingIndicator();
        LoaderManager.LoaderCallbacks<City> callbacks = new WeatherCallbacksReload();
        switch (city.getName()) {
            case "Kazan":
                getSupportLoaderManager().restartLoader(R.id.weather_loader_id_1, Bundle.EMPTY, callbacks);
                break;
            case "Berlin":
                getSupportLoaderManager().restartLoader(R.id.weather_loader_id_2, Bundle.EMPTY, callbacks);
                break;
            case "London":
                getSupportLoaderManager().restartLoader(R.id.weather_loader_id_3, Bundle.EMPTY, callbacks);
                break;
            case "Moscow":
                getSupportLoaderManager().restartLoader(R.id.weather_loader_id_4, Bundle.EMPTY, callbacks);
                break;
            case "Washington":
                getSupportLoaderManager().restartLoader(R.id.weather_loader_id_5, Bundle.EMPTY, callbacks);
                break;
        }
    }


    private void loadWeather(boolean restart) {
        mLoadingView.showLoadingIndicator();
        LoaderManager.LoaderCallbacks<City> callbacks = new WeatherCallbacks();
        if (restart) {
            getSupportLoaderManager().restartLoader(R.id.weather_loader_id_1, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().restartLoader(R.id.weather_loader_id_2, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().restartLoader(R.id.weather_loader_id_3, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().restartLoader(R.id.weather_loader_id_4, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().restartLoader(R.id.weather_loader_id_5, Bundle.EMPTY, callbacks);
        } else {
            getSupportLoaderManager().initLoader(R.id.weather_loader_id_1, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().initLoader(R.id.weather_loader_id_2, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().initLoader(R.id.weather_loader_id_3, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().initLoader(R.id.weather_loader_id_4, Bundle.EMPTY, callbacks);
            getSupportLoaderManager().initLoader(R.id.weather_loader_id_5, Bundle.EMPTY, callbacks);
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("WeatherList Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private class WeatherCallbacksReload implements LoaderManager.LoaderCallbacks<City> {

        @Override
        public Loader<City> onCreateLoader(int id, Bundle args) {
            switch (id) {
                case R.id.weather_loader_id_1:
                    return new RetrofitWeatherLoaderKazan(WeatherListActivity.this, "Kazan");

                case R.id.weather_loader_id_2:
                    return new RetrofitWeatherLoaderBerlin(WeatherListActivity.this, "Berlin");

                case R.id.weather_loader_id_3:
                    return new RetrofitWeatherLoaderLondon(WeatherListActivity.this, "London");

                case R.id.weather_loader_id_4:
                    return new RetrofitWeatherLoaderMoscow(WeatherListActivity.this, "Moscow");

                case R.id.weather_loader_id_5:
                    return new RetrofitWeatherLoaderWashington(WeatherListActivity.this, "Washington");

            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<City> loader, City city) {
            mLoadingView.hideLoadingIndicator();
            startActivity(WeatherActivity.makeIntent(WeatherListActivity.this, city));
        }

        @Override
        public void onLoaderReset(Loader<City> loader) {

        }
    }

    private class WeatherCallbacks implements LoaderManager.LoaderCallbacks<City> {

        @Override
        public Loader<City> onCreateLoader(int id, Bundle args) {
            switch (id) {
                case R.id.weather_loader_id_1:
                    return new RetrofitWeatherLoaderKazan(WeatherListActivity.this, "Kazan");

                case R.id.weather_loader_id_2:
                    return new RetrofitWeatherLoaderBerlin(WeatherListActivity.this, "Berlin");

                case R.id.weather_loader_id_3:
                    return new RetrofitWeatherLoaderLondon(WeatherListActivity.this, "London");

                case R.id.weather_loader_id_4:
                    return new RetrofitWeatherLoaderMoscow(WeatherListActivity.this, "Moscow");

                case R.id.weather_loader_id_5:
                    return new RetrofitWeatherLoaderWashington(WeatherListActivity.this, "Washington");

            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<City> loader, City city) {
            mLoadingView.hideLoadingIndicator();
        }

        @Override
        public void onLoaderReset(Loader<City> loader) {

        }
    }

    @NonNull
    private List<City> getInitialCities() {
        List<City> cities = new ArrayList<>();
        String[] initialCities = getResources().getStringArray(R.array.initial_cities);
        for (String city : initialCities) {
            cities.add(new City(city));
        }
        return cities;
    }
}
