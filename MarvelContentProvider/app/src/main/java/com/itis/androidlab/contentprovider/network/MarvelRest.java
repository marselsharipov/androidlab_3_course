package com.itis.androidlab.contentprovider.network;


import com.itis.androidlab.contentprovider.models.FullComicsInfo;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MarvelRest {
    @GET("/v1/public/comics?startYear=2007")
    Call<FullComicsInfo> getTemperatureByCity();
}
