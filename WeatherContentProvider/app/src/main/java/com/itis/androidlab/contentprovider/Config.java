package com.itis.androidlab.contentprovider;

public class Config {
    public static final String WEATHER_ENDPOINT_DEBUG = "http://api.openweathermap.org/";
    public static final String WEATHER_ENDPOINT_RELEASE = WEATHER_ENDPOINT_DEBUG;
    public static final String APPLICATION_ID = "1f444b7ef3c405864b7b52819c847763";
}
