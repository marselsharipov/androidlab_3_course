package com.itis.androidlab.contentprovider.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Temp {

    public Temp() {
    }


    @JsonProperty("min")
    private Double tempMin;
    @JsonProperty("max")
    private Double tempMax;
    @JsonProperty("eve")
    private Double tempEve;
    @JsonProperty("night")
    private Double tempNight;

    public Double getTempMin() {
        return tempMin;
    }

    public void setTempMin(Double tempMin) {
        this.tempMin = tempMin;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public void setTempMax(Double tempMax) {
        this.tempMax = tempMax;
    }

    public Double getTempNight() {
        return tempNight;
    }

    public void setTempNight(Double tempNight) {
        this.tempNight = tempNight;
    }

    public Double getTempEve() {

        return tempEve;
    }

    public void setTempEve(Double tempEve) {
        this.tempEve = tempEve;
    }
}
