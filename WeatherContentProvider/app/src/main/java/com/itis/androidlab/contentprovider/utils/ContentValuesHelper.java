package com.itis.androidlab.contentprovider.utils;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import com.itis.androidlab.contentprovider.models.FullWeatherInfo;
import com.itis.androidlab.contentprovider.provider.WeatherContract;
import com.itis.androidlab.contentprovider.provider.WeatherContract.Weather;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;


public final class ContentValuesHelper {

    public static ContentValues[] mapTagToContentValues(@NonNull FullWeatherInfo fullWeatherInfo) {
        ContentValues[] bulkToInsert;
        List<ContentValues> mValueList = new ArrayList<>();
        for (int i = fullWeatherInfo.getListofWeather().size() - 1; i > -1; i--) {
            ContentValues cvs = new ContentValues();
            cvs.put(WeatherContract.Weather.TEMP_EVE, fullWeatherInfo.getListofWeather().get(i).getTemp().getTempEve());
            cvs.put(Weather.TEMP_MAX, fullWeatherInfo.getListofWeather().get(i).getTemp().getTempMax());
            cvs.put(WeatherContract.Weather.TEMP_MIN, fullWeatherInfo.getListofWeather().get(i).getTemp().getTempMin());
            cvs.put(WeatherContract.Weather.TEMP_NIGHT, fullWeatherInfo.getListofWeather().get(i).getTemp().getTempNight());
            mValueList.add(cvs);
        }
        bulkToInsert = new ContentValues[mValueList.size()];
        mValueList.toArray(bulkToInsert);
        return bulkToInsert;
    }

    public static ContentValues mapTagToContentValues(Response raw) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(WeatherContract.Error.ERROR_CODE, raw.code());
        contentValues.put(WeatherContract.Error.ERROR_MESSAGE, raw.message());
        contentValues.put(WeatherContract.Error.ERROR_PROTOCOL, raw.protocol().toString());
        return contentValues;
    }
}
