package com.itis.androidlab.contentprovider.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itis.androidlab.contentprovider.R;
import com.itis.androidlab.contentprovider.models.Temp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FullWeatherInfoAdapter extends RecyclerView.Adapter<FullWeatherInfoAdapter.ViewHolder> {

    private List<Temp> mTemp = new ArrayList<>();

    @Override
    public FullWeatherInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_tag, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FullWeatherInfoAdapter.ViewHolder holder, int position) {
        Temp temp = mTemp.get(position);
        holder.mOneElement.setText(String.format(holder.mStringFormatAllTime, "Макс", temp.getTempMax().toString()));
        holder.mTwoElement.setText(String.format(holder.mStringFormatAllTime, "Мин", temp.getTempMin().toString()));
        holder.mThreeElemt.setText(String.format(holder.mStringFormatAllTime, "Вечер", temp.getTempEve().toString()));
        holder.mFourElement.setText(String.format(holder.mStringFormatAllTime, "Ночь", temp.getTempNight().toString()));
        holder.mDate.setText(Integer.toString(calendarHelper().get(Calendar.DAY_OF_MONTH) + position) + "." + (Integer.toString(calendarHelper().get(Calendar.MONTH) + 1)) + "." + (Integer.toString(calendarHelper().get(Calendar.YEAR))));

    }


    private Calendar calendarHelper() {
        return Calendar.getInstance();
    }

    @Override
    public int getItemCount() {
        return mTemp.size();
    }

    public void setmTemp(List<Temp> mTemp) {
        this.mTemp = mTemp;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindString(R.string.string_format_all_time) String mStringFormatAllTime;
        @BindView(R.id.one_element) TextView mOneElement;
        @BindView(R.id.date) TextView mDate;
        @BindView(R.id.two_element) TextView mTwoElement;
        @BindView(R.id.three_elemet) TextView mThreeElemt;
        @BindView(R.id.four_element) TextView mFourElement;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
