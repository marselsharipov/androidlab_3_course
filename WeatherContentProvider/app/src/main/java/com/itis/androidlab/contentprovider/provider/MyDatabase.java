package com.itis.androidlab.contentprovider.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Helper for managing {@link SQLiteDatabase} that stores data for
 * {@link WeatherProvider}.
 */
public class MyDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "tags.db";
    private static final int DATABASE_VERSION = 8;

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    interface Tables {
        String WEATHER = "weather";
        String ERROR = "error";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.WEATHER + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + WeatherContract.Weather.TEMP_NIGHT + " REAL NOT NULL,"
                + WeatherContract.Weather.TEMP_MAX + " REAL NOT NULL,"
                + WeatherContract.Weather.TEMP_MIN + " REAL NOT NULL,"
                + WeatherContract.Weather.TEMP_EVE + " REAL NOT NULL)");

        db.execSQL("CREATE TABLE " + Tables.ERROR + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + WeatherContract.Error.ERROR_CODE + " REAL NOT NULL,"
                + WeatherContract.Error.ERROR_MESSAGE + " TEXT NOT NULL,"
                + WeatherContract.Error.ERROR_PROTOCOL + " TEXT NOT NULL)");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop tables that have been deprecated.
        db.execSQL("DROP TABLE IF EXISTS " + Tables.WEATHER);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.ERROR);
        onCreate(db);
    }
}