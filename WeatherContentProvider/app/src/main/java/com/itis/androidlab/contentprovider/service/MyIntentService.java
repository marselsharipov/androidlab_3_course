package com.itis.androidlab.contentprovider.service;

import android.app.IntentService;
import android.content.Intent;

import com.itis.androidlab.contentprovider.models.FullWeatherInfo;
import com.itis.androidlab.contentprovider.network.SessionRestManager;
import com.itis.androidlab.contentprovider.provider.WeatherContract;
import com.itis.androidlab.contentprovider.utils.ContentValuesHelper;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;


public class MyIntentService extends IntentService {


    public MyIntentService() {
        super("IntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Call<FullWeatherInfo> call = SessionRestManager.getInstance().getRest().getTemperatureByCity("Kazan", "metric", 10);
        try {
            Response<FullWeatherInfo> response = call.execute();
            FullWeatherInfo fullWeatherInfo = response.body();

            getContentResolver().delete(WeatherContract.Weather.CONTENT_URI, null, null);
            getContentResolver().bulkInsert(WeatherContract.Weather.CONTENT_URI, ContentValuesHelper.mapTagToContentValues(fullWeatherInfo));

            getContentResolver().insert(WeatherContract.Error.CONTENT_URI, ContentValuesHelper.mapTagToContentValues(response.raw()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}