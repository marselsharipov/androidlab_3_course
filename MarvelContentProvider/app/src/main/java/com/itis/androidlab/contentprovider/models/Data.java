package com.itis.androidlab.contentprovider.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    @JsonProperty("results")
    private List<ListOfComics> listOfComics;


    public List<ListOfComics> getListOfComics() {
        return listOfComics;
    }

    public void setListOfComics(List<ListOfComics> listOfComics) {
        this.listOfComics = listOfComics;
    }
}
